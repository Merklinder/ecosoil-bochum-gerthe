# Email an den Radverkehrsbeauftragten von Straßen.NRW
* Betreff: gefährlicher Streckenabschnitt in Bochum (Bövinghauser Hellweg)
* Gesendet an: [den Radverkehrsbeauftragten für die Region Ruhr, Hr. Schmittem](https://www.strassen.nrw.de/de/wir-bauen-fuer-sie/projekte/radwege/radverkehrsbeauftragte.html)
* Gesendet am: 29.09.2020

Sehr geehrter Herr Schmittem,

als Anwohner der Gerther Straße in Castrop-Rauxel und als Radfahrer mit
Pendelstrecke u.a. über den Bövinghauser Hellweg in Bochum Gerthe wende
ich mich an Sie, da sich die Verkehrssituation auf meiner Pendelstrecke
zunehmend verschärft.

Kartenausschnitt: https://bit.ly/3cEtn63

Auf diesem Streckenabschnitt gibt es aus meiner Sicht zur Zeit mehrere
gravierende Gefahren für Radfahrende:

* Es gibt es weder einen Radweg, noch einen befestigten Bürgersteig auf
welchen man ausweichen könnte

* Der Abschnitt ist bei Dunkelheit nicht ausreichend beleuchtet

* Der bisherige LKW-Verkehr (durch Remondis und das Gewerbegebiet
BO-Gerthe Nord) hat der Straße bereits stark zugesetzt. Besonders im
Bereich der Straßenabläufe (Gullies) sind die Straßenschäden derart
ausgeprägt, dass man als Radfahrender weit in die Fahrspurmitte
ausweichen muss - oft zur "Überraschung" des nachfolgend Verkehrs

* die unbebaute Umgebung scheint bei Autofahrenden zu überhöhten
Geschwindigkeiten einzuladen

* der Kontaktstreifen an der Ampel Bövinghauser Hellweg/ Dieselstraße
reagiert nicht auf Fahrräder, so dass hier "über rot" gefahren werden
muss (sofern nicht bereits wartende Autos an der Ampel stehen)

Neben den genannten Punkten wird nun noch eine weitere Firmenansiedlung
mit zusätzlichen 300 LKW-Bewegungen pro Tag geplant¹, wodurch sich die
ohnehin gefährliche Verkehrslage auf dem Abschnitt nochmals deutlich
verschärfen würde.

Ich weiß nicht ob Sie als Radverkehrsbeauftragter für die Region Ruhr
der richtige Ansprechpartner sind und welchen Handlungsspielraum Sie
hier sehen - aber über eine Rückmeldung zu diesem Thema würde ich mich
freuen!

PS: Für eine Ortsbegehung und/ oder die Nachreichung weiterer
Informationen (Fotos, etc.) stehe ich Ihnen bei Bedarf gerne zur Verfügung!

¹
https://test.soziale-liste-bochum.de/wp-content/uploads/2020/08/61.19.11.Bodenbehandlungsanlage_Stellungnahme.pdf