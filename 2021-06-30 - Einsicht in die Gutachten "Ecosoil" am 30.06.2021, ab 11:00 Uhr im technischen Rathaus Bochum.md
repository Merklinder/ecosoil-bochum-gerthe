# Einsicht in die Gutachten "Ecosoil" am 30.06.2021, ab 11:00 Uhr im technischen Rathaus Bochum

## Allgemeine Informationen

* 50 Mitarbeitende sollen am Standort beschäftigt werden

## Verkehrsgutachten

*vom 27.04.2021, erstellt durch [ambrosius blanke](https://www.ambrosiusblanke.de/)*

* die erste Messung erfolgte am 16.06.2020
* eine zweite Messung erfolgte am 12.01.2021
* die Spitzenwerte lagen...
	* ...am Morgen im Zeitraum von 6:45 Uhr bis 7:45 Uhr bei 369 Kfz/h
	* ...am Nachmittag im Zeitraum von 16 - 17 Uhr bei 548 Kfz/h
* die Werte wurden im Corna-Zeitraum gemessen und für die weitere Betrachtung pauschal um 30% erhöht
* Durch Ecosoil geplante Fahrten:
	* 250 Anlieferungen pro Tag
		* davon 10-15% Klein-LKW bis 7,5 to., Absetz- und Rollcontainer-Fahrzeuge, sowie 2-, 3- oder 4-Achser
		* die verbleibenden 85-90% Fahrzeuge sind Tandem- und Sattelzüge
	* 150 Abfuhren pro Tag, davon 100% Sattelzüge und 4-Achser (25 to. Sattelkipper)
* Der innerbtriebliche Transport erfolgt per Radlader
* Für den Knotenpunkt B235/Gerther Straße liegen keine "signaltechnischen" Unterlagen vor, so dass keine detaillierte Berechnung der Leistungsfähigkeit und der "verkehrlichen Kenngrößen" (mittlere Wartezeiten, Staulängen, usw.) des Knotenpunkts nach HBS nicht vorgenommen werden kann.

## Staubemissionsgutachten

*vom 25.03.2020, erstellt durch [Uppenkamp + Partner GmbH](https://www.uppenkamp-partner.de/)*

## Lärmgutachten

*vom 25.03.2020, erstellt durch [ITAB GmbH](https://itab.de/)*

* erforderlich: 3,5 Meter hohe Lärmschutzwand an der nördlichen Grundstücksgrenze
* erforderlich: 3 Meter hohe Lärmschutzwand an der nördlichen Grundstücksgrenze
* Laut Ecosoil sollen die Bödem am Standort nur gesiebt; nicht gebrochen werden
* Bereits jetzt (im Bestand; Conona-Zeitraum?) überschreitet der Knotenpunkt die Geräuschemissionsgrenzwerte der Verkehrslärmschutzverordnung um 2,3 dB(A)

## FEHLT (noch nicht angefragt): Befahrbarkeitsuntersuchung

*erstellt durch das Tiefbauamt der Stadt Bochum, Abteilung Straßen*

## NOCH AUSSTEHEND: Artenschutzprüfung (ASP)

*erfolgt erst im BImSch-Verfahren*

## Angefragt: Bescheid über die Bauvoranfrage "Ecosoil"

* An: XXX@Bochum.de
* Betreff: Bescheid über die Bauvoranfrage "Ecosoil"
* gesendet: 30.06.2021

> Guten Tag Frau XXX,
> 
> wie heute mit Ihnen besprochen, bitte ich auf diesem Weg erneut um Zusendung des Vorbescheids samt Rechtsbehelfsbelehrung.
> 
> Zur Wahrung unserer Rechte als betroffene Anwohner:innen (hier konkret: Möglichkeit der Klageerhebung gegen den Bauvorbescheid) habe ich bereits am 17.05.2021 den "Bescheid über die Bauvoranfrage gemäß Mitteilung der Verwaltung, Vorlage Nr.: 20201399 'Neuerrichtung eines Bodenbehandlungsbetriebes' (Aktenzeichen 63 22 (3037))" angefordert: https://fragdenstaat.de/a/220527
> 
> Zum 19.06.2021 habe ich noch keine Antwort auf meine Anfrage erhalten, so dass ich meine Anfrage am selben Tag erneuerte. Heute, am 30.06.2021 habe ich - im Rahmen der Einsicht in die Gutachten zum Verfahren - erneut nach dem Bauvorbescheid gefragt; dieser lag jedoch nicht zur Einsicht vor.
> 
> Für Ihre Mühen vielen Dank im Voraus!
> 
> Die "Befahrbarkeitsuntersuchung"
