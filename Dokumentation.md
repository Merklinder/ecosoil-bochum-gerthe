# Ecosoil Nord-West GmbH - Umsiedlung des Betriebs nach Bochum Gerthe

## Worum geht's?

- Das Unternehmen [ECOSOIL Nord-West GmbH](http://www.ecosoil-umwelt.de/ecosoil-nord-west-gmbh) (nachfolgend "Ecosoil") plant die Umsiedlung ihres Betriebs innerhalb des Bochumer Stadtgebiets von der [Rensingstr. 14](https://www.openstreetmap.org/way/653754138#map=17/51.51688/7.20749) zur [Bövinghauser Straße 50-58](https://www.openstreetmap.org/way/87408014#map=16/51.5245/7.3168)
    - Ecosoil sucht einen neuen Standort, [da das Handelsunternehmen Lidl am jetzigen Standort ein Warenverteilzentrum plant](https://www.bochum.de/Pressemeldungen/24-Januar-2020/Lidl-entwickelt-Flaeche-fuer-neues-Lebensmittelverteilzentrum-HerBo43-auf-Stadtgrenze-Herne-/-Bochum).
    - aktuelle Pächterin des Grundstücks ist die "Philippine GmbH & Co. Dämmstoffsysteme KG" (dies wurde mir am 27.06.2021 in einem persönlichen Gespräch mitgeteilt)
- Der neue Standort [grenzt unmittelbar an die Stadtgrenze zu Castrop-Rauxel](https://www.openstreetmap.org/way/87408014#map=19/51.52358/7.31123) (Stadtteil Merklinde)
- Ecosoil hat einen weiteren Unternehmensstandort im Gebiet der Stadt Castrop-Rauxel ([Deininghauser Weg 81](https://www.openstreetmap.org/way/512587691#map=17/51.58046/7.33428))
- Die [Pendelstrecke zwischen den beiden Standorten](https://maps.openrouteservice.org/directions?n1=51.551742&n2=7.327316&n3=13&a=51.522926,7.31032,51.580528,7.334123&b=0&c=0&k1=en-US&k2=km) findet ausschließlich auf Castroper Stadtgebiet statt
- Die [Ruhrnachrichten schrieben von "bis zu 300 Lastwagen **mehr** pro Tag" zwischen 6 und 22 Uhr](https://www.ruhrnachrichten.de/castrop-rauxel/castrop-rauxel-will-betriebs-ansiedlung-an-der-stadtgrenze-verhindern-plus-1531285.html)

## Kritik am Vorhaben

Als Anwohner der Nachbarschaft sehe mit dem geplanten Vorhaben schädliche Umwelteinwirkungen und sonstige Gefahren, sowie erhebliche Nachteile und erhebliche Belästigungen der Allgemeinheit und der Nachbarschaft verbunden. Daher halte ich die Überprüfung des Vorhabens und die öffentliche Beteiligung im Rahmen eines Genehmigungsverfahrens nach § 10 BImSchG für erforderlich. Insbesondere folgende Punkte möchte ich zur Begründung anbringen:

1. Verkehrsbelastung
    * 300 Lastwagen zusätzlich pro Tag zwischen 6 und 22 Uhr = 300 LKW in 16 Stunden = 18,75 LKW pro Stunde = 1 LKW alle 3,2 Minuten zwischen 6 und 22 Uhr
    * stark erhöhtes Unfallrisiko auf dem Bövinghauser Hellweg: Dort gibt es über weite Strecken weder Rad- noch Fußwege neben der Straße. Fußgänger, Radfahrer und motorisierter (LKW-)Verkehr teilen sich die Straße.
    * Die Verkehrsbelastung auf der Gerther Straße (speziell am Knotenpunkt Wittener Straße/B235) ist bereits jetzt so hoch, dass sich in Stoßzeiten der Verkehr über mehrere hundert Meter zurück staut. 300 LKW mehr pro Tag sind - aus meiner Sicht - hier nicht akzeptabel.
2. Lärmelästigung
    * durch die Verarbeitung (brechen/ sieben) von Böden (Gestein)
    * durch 300 Lastwagen zusätzlich pro Tag zwischen 6 und 22 Uhr
3. (Fein-)Staubbelastung
    * durch die Verarbeitung (brechen/ sieben) von Böden (Gestein)
    * durch die Abgase von 300 Lastwagen zusätzlich pro Tag zwischen 6 und 22 Uhr
4. Geruchsbelästigung
    * abhängig vom zu verarbeitenden Boden
5. Beeinträchtigung durch Vibration
    * durch die Verarbeitung (brechen/ sieben) von Böden (Gestein)
    * durch 300 Lastwagen zusätzlich pro Tag zwischen 6 und 22 Uhr

## Zuständigkeiten

* Bauherrin: [ECOSOIL Nord-West GmbH](https://www.ecosoil-umwelt.de/unternehmen/ecosoil-nord-west)
* Pächterin: [Philippine GmbH & Co. Dämmstoffsysteme KG](https://www.philippine-eps.de/)
* Untere Bauaufsichtsbehörde (Beurteilung gemäß § 35 Abs. 2 BauGB ("Vorbescheid")): [Bauordnungsamt der Stadt Bochum](https://www.bochum.de/Bauordnungsamt)
* Obere Bauaufsichtsbehörde (Antrag nach § 4 BimSchG): [Bezirksregierung Arnsberg, Dezernat 52, Abfallwirtschaft](https://www.bra.nrw.de/system/files/media/document/file/Orgaplan%202021-06-15.pdf)
* Oberste Bauaufsichtsbehörde: [Ministerium für Heimat, Kommunales, Bau und Gleichstellung des Landes Nordrhein-Westfalen ("Bauministerium NRW")](https://www.mhkbg.nrw/)

## Historie

* 04.06.2020 - Mitteilung der Verwaltung über die geplante Neuerrichtung eines Bodenbehandlungsbetriebes ([Vorlage Nr.: 20201399](https://session.bochum.de/bi/getfile.asp?id=452399&type=do&#search=%2220201399%22))
   * Die  Bauvoranfrage  wurde  am  31.03.2020  bei  der  Stadt  Bochum eingereicht. (vgl. Beschlussvorlage der Verwaltung Nr. 20211595, Seite 3, Absatz 1)
   * Es wurden ein Geräuschgutachten und eine Vorabschätzung der Staubimmissionen zur Prüfung vorgelegt. Des Weiteren sollte noch ein Verkehrsgutachten aufgestellt werden.
   * 09.06.2020: In der [55. Sitzung der Bezirksvertretung](https://session.bochum.de/bi/getfile.asp?id=454099&type=do&#search=%2220201399%22) Bochum-Nord wird einstimmig beschlossen den Tagesordnungspunkt nicht in die Tagesordnung aufzunehmen. Es fehlen die Ergebnisse der in der Mitteilung erwähnten Lärm-, Immissions- und Verkehrsgutachten.
   * 01.09.2020: In der [57. Sitzung der Bezirksvertretung](https://session.bochum.de/bi/getfile.asp?id=456815&type=do&#search=%2220201399%22) Bochum-Nord wird festgestellt, dass die Gutachten noch immer nicht vorliegen. Die Vorlage wir "ablehnend" zur Kenntnis genommen.
* 27.07.2020 - [(erste) Stellungnahme der Stadt Castrop-Rauxel](https://test.soziale-liste-bochum.de/wp-content/uploads/2020/08/61.19.11.Bodenbehandlungsanlage_Stellungnahme.pdf)
* 25.08.2020 - Anfrage der Sozialen Liste im Rat ([Vorlage Nr. 20202170](https://session.bochum.de/bi/getfile.asp?id=457208&type=do))
* 04.09.2020 - [Demonstrationszug in Castrop-Rauxel gegen die Ansiedlung der Firma "Ecosoil"](http://wir-sind-merklinde.de/?p=1003) ([Video von Cas-TV.de](https://www.nrwision.de/mediathek/cas-tv-laerm-macht-krank-demo-gegen-lkw-laerm-in-castrop-rauxel-merklinde-200915/))
* 09.09.2020 - Information des Vorhabens an den Bezirksältestenrat Bochum Nord durch den Stadtbaurat und den Leiter des Bauordnungsamtes
  * [Die Informationen wurden laut Bauordnungsamt nur mündlich vorgetragen; ohne weitere Dokumentation](https://fragdenstaat.de/anfrage/dokumentation-der-politischen-beteiligung-im-rahmen-der-ansiedlung-von-ecosoil-in-bochum-gerthe/)
* 14.10.2020 - Antwort der Verwaltung auf die Anfrage der „Soziale Liste im Rat“ vom 25.08.2020 ([Vorlage Nr. 20202323](https://session.bochum.de/bi/getfile.asp?id=458368&type=do))
  * wichtig darin: »Die Anlage unterliegt den Vorschriften des Bundes-Immissionsschutzgesetz. Die Genehmigung zur Errichtung und zum Betrieb der Anlage wird nicht von der Stadt Bochum erteilt. Das Verfahren wird von der Bezirksregierung Arnsberg geführt.«
* 16.10.2020, ab 15:00 Uhr: [Protestmarsch mit Kundgebung zur geplanten Bodenaufbereitungsanlage und zu Gerthe West](https://gerthe-west-so-nicht.de/protestmarsch-mit-kundgebung-zur-geplanten-bodenaufbereitungsanlage-und-zu-gerthe-west/)
* 27.10.2020 - persönliches Treffen mit dem Leiter der Bereiche Stadtplanung und Bauordnung der Stadt Castrop-Rauxel, Herrn Philipp Röhnert, Herr Kelling (Amtsleiter des Bauordnungsamtes der Stadt Bochum) und Frau Bartel als Sachbearbeiterin des Vorgangs
   * bislang undokumentiert, [Bochum hat angeblich nichts](https://fragdenstaat.de/anfrage/personliches-gesprach-der-zustandigen-behorden-aus-castrop-rauxel-und-bochum-bezuglich-der-betriebsansiedlung-eco-soil-in-bochum-gerthe/), [Castrop hat noch nicht geantwortet](https://fragdenstaat.de/anfrage/personliches-gesprach-der-zustandigen-behorden-aus-castrop-rauxel-und-bochum-bezuglich-der-betriebsansiedlung-eco-soil-in-bochum-gerthe-1/)
* 09.11.2020 - Anregung der Bezirksfraktion Bochum-Nord zur Verhinderung weiterer Industrieansiedlungen im Bochumer Norden ([Vorlage Nr. 20202696](https://session.bochum.de/bi/getfile.asp?id=461253&type=do))
* 10.02.2021 - Lesenswerte Anfrage der SPD-Bezirksfraktion Bochum-Nord an die Bezirksverwaltung Bochum-Nord ([Vorlage Nr. 20210408](https://session.bochum.de/bi/getfile.asp?id=467333&type=do))
  1. Liegen diese Gutachten vollständig vor?
  1. Wer hat die Gutachten mit welchen Ergebnissen erstellt?
  1. Wie bewertet die Verwaltung die Ergebnisse der Gutachten?
  1. Prüft die Verwaltung die Gutachten auf ihre Plausibilität (Datenbasis, Rückschlüsse usw.)?
  1. Wurde beim Verkehrsgutachten (Ecosoil) ein Abgleich mit dem Verkehrsgutachten Bochum-Nord vorgenommen?
  1. Durch welche verkehrlichen/baulichen Maßnahmen soll eine Erschließung für den Schwerlastverkehr hergestellt werden?
  1. Wie beabsichtigt die Verwaltung mit der Anregung umzugehen?
  1. Wann wird die Anregung dem Rat bzw. dem Ausschuss für Planung und Grundstücke vorgelegt?
* 29.04.2021 - Mitteilung über das Ergebnis der Prüfung der Bauvoranfrage ("positiver Bescheid") an den Bezirksältestenrat Bochum Nord durch den Stadtbaurat und den Leiter des Bauordnungsamtes ([siehe hier, Seite 5](https://session.bochum.de/bi/getfile.asp?id=475885&type=do))
* 12.05.2021 - (zweite) [Stellungnahme der Stadt Castrop-Rauxel an die Stadt Bochum](https://fragdenstaat.de/anfrage/schreiben-vom-12052021-an-die-bochumer-verwaltung-zum-verkehrslenkung-erschlieung-ecosoil/602551/anhang/20210512_Stellungnahme_Bodenbehandlungsanlage_ecosoil_geschwaerzt.pdf) zur Bauvoranfrage der Firma Ecosoil
* 18.05.2021 - Beschlussvorlage der Verwaltung zur Ablehnung der Anregung der Bezirksvertretung Bochum Nord vom 09.11.2020 ([Nr. 20211595](https://session.bochum.de/bi/getfile.asp?id=474707&type=do))
   * vermutlich beschlossen in der Ratssitzung vom 27.05.2021 (das Protokoll wurde Stand 08.06.2021 noch nicht veröffentlicht)
* 27.05.2021 - Anfragen der Grünen zu möglichen Umweltauflagen ([Vorlage 20211770](https://session.bochum.de/bi/getfile.asp?id=476068&type=do))
   * noch unbeantwortet (Stand: 05.07.2021)
* 24.05.2021: [Anfechtung der Rechtmäßigkeit der Ecosoil-Ansiedlung durch DIE LINKE (Fraktion im Rat der Stadt Castrop-Rauxel) bei der Bezirksregierung Arnsberg](https://www.die-linke-castrop-rauxel.de/wp-content/uploads/2021/06/Ecosoil_Kommunalaufsicht_Arnsberg-1.pdf)
* 02.06.2021 - Antwort der Verwaltung an die Anfrage der Bezirksvertretung Bochum-Nord vom 10.02.2021 ([Vorlage Nr.: 20211758](https://session.bochum.de/bi/getfile.asp?id=475885&type=do))
   * wichtig darin: »Dem Antragsteller [Ecosoil] wird empfohlen, eine Infoveranstaltung im Rahmen des Genehmigungsverfahrens nach § 4 Bundesimmissionsschutzgesetz (BImSchG), in Abstimmung mit der Bezirksregierung Arnsberg durchzuführen. Am 29.04.2021 wurde der Bezirksältestenrat darüber informiert, dass ein positiver Bescheid erteilt wird.«
* [02.06.2021 - Beschluss des Rats über den Antrag zur Beauftragung der Stadtverwaltung Castrop-Rauxel](https://castroprauxel.more-rubin1.de/vorlagen_details.php?vid=292505100172)
   * Ankündigung eines Protestmarsches am 25.06.2021 ab 15 Uhr vom Merklinder Ortskern bis zum geplanten Standort
* 08.06.2021: Frau Schneidermeier liegt noch kein Antrag nach § 4 Bundes-Immissionsschutzgesetz (BImSchG) der Fa. Ecosoil vor (telefonische Auskunft)
* 09.06.2021: [E-Mail an Frau Schniedermeier (Bezirksregierung Arnsberg)](https://gitlab.com/Merklinder/ecosoil-bochum-gerthe/-/raw/master/2021-06-09%20-%20E-Mail%20an%20Frau%20Schniedermeier%20(Bezirksregierung%20Arnsberg))
* 10.06.2021: [Stellungnahme der Stadt Castrop-Rauxel an die Bezirksregierung Arnsberg](https://eservice2.gkd-re.de/selfdbinter040/DokumentServlet?dokumentenname=325-1520fieldDownloaddokument.pdf)
* 15.06.2021: [FDP Castrop-Rauxel fordert (leider nur auf Facebook?):](https://www.facebook.com/fdpcastrop/posts/1816561128536521)
    * die Stadt Castrop-Rauxel solle feststellen, ob eine Erschließung über Bövinghauser Straße in Frage kommt
    * die Stadt Castrop-Rauxel solle eine Gewichtsbeschränkung bis maximal 7,5 Tonnen (mit Ausnahmen für landwirtschaftliche Fahrzeuge) anordnen und ausschildern
* 21.06.2021: Stellungnahme der Rechtsanwältin Dr. Baars von der Rechtsanwälte Partnerschaft mdB Wolter Hoppenberg auf Anfrage der Stadtverwaltung
	* Der erteilte Bauvorantrag ist sei nicht zulässig, da das Vorhaben nach Bundes-Immissionsschutzgesetz hätte beantragt werden müssen
		* Zitat: »Der Vorbescheid ist damit unheilbar rechtswidrig. Für ein späteres Genehmigungsverfahren bei der Bezirksregierung Arnsberg ist er unbeachtlich.«
	* Die Bekanntgabe des Bescheids gegenüber der Stadt Castrop erfolgte ohne Rechtsbehelfsbelehrung. Dadurch verlängert sich die Frist zur »Einlegung des Rechtsbehelfs« (Klagefrist) auf ein Jahr (statt einem Monat).
* 24.06.2021: Frau Schniedermeier (Bezirksregierung Arnsberg) hat auf Nachfrage per E-Mail mitgeteilt, dass noch kein Antrag durch Ecosoil eingegangen sei
* 24.06.2021: [Sitzung des Rates der Stadt](https://castroprauxel.more-rubin1.de/meeting.php?sid=2021-RAT-126)
	* [Antrag der FDP](https://castroprauxel.more-rubin1.de/vorlagen_details.php?vid=292306100209)
        * hier wird insbesondere die Erschließung angezweifelt
	* [Antrag der SPD und Grünen](https://castroprauxel.more-rubin1.de/vorlagen_details.php?vid=292406100210)
		* darin interessant: es gibt eine juristische Einschätzung Kanzlei Wolter Hoppenberg vom 21.06.2021 zum Thema Ecosoil
* 25.06.2021: [zweite Demonstration "Lärm macht krank!"](http://wir-sind-merklinde.de/?p=1113)
* 27.06.2021: [IFG-Anfrage "Aufstellung aller Termine der Stadtverwaltung Castrop-Rauxel mit Verfahrensbeteiligten"](https://fragdenstaat.de/a/224025)
  * Am 25.06.2021 wurde mir mündlich mitgeteilt, dass Vertreter der Firmen Ecosoil (Bauherrin) und Philippine (Pächterin) ein persönliches Gespräch mit dem Bürgermeister der Stadt Castrop-Rauxel zum Thema hatten
* 27.06.2021: [IFG-Anfrage "Stellungnahme der Rechtsanwälte Partnerschaft mdB Wolter Hoppenberg"](https://fragdenstaat.de/anfrage/umsiedlung-ecosoil-nord-west-gmbh-stellungnahme-der-rechtsanwalte-partnerschaft-mdb-wolter-hoppenberg/)
* 27.06.2021: [IFG-Anfrage "Ergebnisse der letzten Straßenverkehrszählung"](https://fragdenstaat.de/anfrage/ergebnisse-der-letzten-straenverkehrszahlung/)
* 28.06.2021: [E-Mail an die Kommunalaufsicht der Bezirksregierung Arnsberg mit Fragen zu Transparenzpflichten bei Verfahren nach § 35 BauGB](https://gitlab.com/Merklinder/ecosoil-bochum-gerthe/-/raw/master/28.06.2021:%20E-Mail%20an%20die%20Kommunalaufsicht%20der%20Bezirksregierung%20Arnsberg%20mit%20Fragen%20zu%20Transparenzpflichten%20bei%20Verfahren%20nach%20%C2%A7%2035%20BauGB.txt)
* 30.06.2021: Einsicht in die Gutachten bei der Stadt Bochum
  * [Fragenkatalog](https://demo.hedgedoc.org/s/ptMaWK5yf#)
  * [Auswertung](https://gitlab.com/Merklinder/ecosoil-bochum-gerthe/-/blob/master/2021-06-30%20-%20Einsicht%20in%20die%20Gutachten%20%22Ecosoil%22%20am%2030.06.2021,%20ab%2011:00%20Uhr%20im%20technischen%20Rathaus%20Bochum.md)
* 06.08.2021: Einreichung der [Klage der Stadt Castrop-Rauxel gegen die Stadt Bochum wegen "Gemeindenachbaranfechtung eines Bauvorbescheids"](https://eservice2.gkd-re.de/selfdbinter040/DokumentServlet?dokumentenname=325-1520fieldDownloaddokument.pdf)
  * Hinweis: Es liegen zwei Klagen beim Verwaltungsgericht vor (Az.: 5K3155 & 5K3471) – eine davon ist jene der Stadt; die andere Klage ist von privat

## Kritik an dem Verfahren der Bauvoranfrage

Das Bauordnungsamt der Stadt Bochum (vertreten durch Frau Bartel) informierte mit [Schreiben vom 04.06.2020 (Vorlage 20201399)](https://session.bochum.de/bi/getfile.asp?id=452399&type=do&#search=%2220201399%22) darüber, dass Ecosoil die Neuerrichtung eines Bodenbehandlungsbetriebes in Bochum Gerthe plant.

Konkret vermute ich, dass es bei dem Verfahren um einen Antrag auf [Vorbescheid nach § 77 BauO NRW 2018](https://recht.nrw.de/lmi/owa/br_bes_detail?sg=0&menu=1&bes_id=39224&anw_nr=2&aufgehoben=N&det_id=468265) handelt, bei welchem die planungsrechtliche Zulässigkeit des Vorhabens beschieden werden sollte.

Die Verwaltung teilt in ihrer [initialen Meldung](https://session.bochum.de/bi/getfile.asp?id=452399&type=do&#search=%2220201399%22) (Seite 2, Absatz 3) mit, dass die Beurteilung nach [§ 35 Abs. 2 BauGB](https://www.gesetze-im-internet.de/bbaug/__35.html) erfolgt.
* § 35 Absatz 2 lautet:
> Sonstige Vorhaben können im Einzelfall zugelassen werden, wenn ihre Ausführung oder Benutzung öffentliche Belange nicht beeinträchtigt und die Erschließung gesichert ist.
* Nach § 35 Absatz 3 gilt jedoch:
> Eine Beeinträchtigung öffentlicher Belange liegt insbesondere vor, wenn das Vorhaben...
>  * Satz 3: »schädliche Umwelteinwirkungen hervorrufen kann« oder
>  * Satz 4: »unwirtschaftliche Aufwendungen für Straßen oder andere Verkehrseinrichtungen, für Anlagen der Versorgung oder Entsorgung, für die Sicherheit oder Gesundheit oder für sonstige Aufgaben erfordert«

Sowohl die potenziell schädlichen Umwelteinwirkungen, als auch die Verkehrsbelastung auf der Gerther Straße / B235 wurden oben bereits aufgeführt ("Kritik am Vorhaben"). Diese Kritikpunkte wurden bislang nicht mit Belegen (Gutachten) entkräftigt.

In einer Beschlussvorlage der Verwaltung heißt es hierzu:

> Besonderes Augenmerk lag auf dem Verkehrsgutachten. Das Gutachten lag der Verwaltung in einem ersten Entwurf am 16.07.2020 vor; die Prüfung wurde am 13.04.2021 inhaltlich abgeschlossen.Im Ergebnis ist festzustellen, dass die Erschließung gesichert ist und das vorhandene Straßennetz und die vorhandenen Knotenpunkte ausreichend leistungsfähig und noch mit weiteren Reserven ausgestattet sind. Auch mit Blick auf Lärm-und Staubemissionen sind keine negativen Auswirkungen zu besorgen.

Dieses Prüfergebnis zweifle ich ausdrücklich an; leider kann ich diese Zweifel (noch) nicht inhaltlich belegen, da mir die Einsicht in die Gutachten bislang nicht gewährt wurde.

Meine [mehrfachen Anfragen an die Stadt Bochum nach den letztgültigen Gutachten](https://fragdenstaat.de/anfrage/betriebsansiedlung-ecosoil-gmbh-in-bochum-gerthe-neue-gutachten/) der Bauherrin sind bislang (Stand: 09.06.2021) unbeantwortet, so dass die interessierte Öffentlichkeit bislang keine Möglichkeit hat, sich detailliert über das Bauvorhaben zu informieren.

Zudem erfolgt die Erschließung des Grundstücks nur zum Teil über Bochum Stadtgebiet; jedoch werden auch 100% des Betriebsverkehrs über ein (kurzes) Stück Castrop-Rauxler Stadtgebiet geführt. Insbesondere die einzige(!) Anschlusstelle der Bövinghauser Straße an die Gerther Straße (Castrop-Rauxel) und den Bövinghauser Hellweg (Bochum) liegt auf Castrop-Rauxler Stadtgebiet. Die Stadt Castrop-Rauxel wurde im Rahmen der Bauvoranfrage - soweit mir bekannt - lediglich über das Vorhaben informiert; jedoch nicht in die Prüfung und Genehmigung eingebunden.

### mangelhafte politische Beteiligung

In der [Vorlage Nr.: 20211595 vom 18.05.2021](https://session.bochum.de/bi/getfile.asp?id=474707&type=do) teilt das Bauordnungsamt der Stadt Bochum mit:

> Aus diesem Grund haben der Stadtbaurat und der Leiter des Bauordnungsamtes den Bezirksältestenrat am 09.09.2020 umfassend über das Vorhaben und das beabsichtigte Verfahren informiert und zugesagt, den Ältestenrat erneut über das Ergebnis der Prüfung der Bauvoranfrage zu informieren, bevor der Bescheid erteilt wird. Dies ist am 20.04.2021 geschehen.«

Auf meine [Anfrage vom 24. Mai 2021](https://fragdenstaat.de/a/220946) nach den Dokumenten der o.g. Treffen teilt das Bauordnungsamt der Stadt Bochum am 28. Juni 2021 mit:

> es tut mir leid, aber ich habe dem Bezirksältestenrat Nord weder am 09.09.2020 noch am 20.04.2021 Dokumente vorgelegt. Verwendet wurden am 20.04.2021 Auszüge aus dem Verkehrsgutachten als Powerpoint-Datei. Ansonsten haben der Leiter des Bauordnungsamtes und ich mündlich vorgetragen.

## mögliche Handlungsoptionen

1. Kann der positive Bescheid über die planungsrechtliche Zulassung gemäß der Bauvoranfrage noch angefochten/ wiederrufen werden?
    * [Widerspruch](https://www.bpb.de/164004/widerspruch)/ [Verwaltungsprozess](https://www.bpb.de/23176/verwaltungsprozess)
    * Welche Gründe könnten hierzu angewandt werden?
    * Welcher formale Weg muss bestritten werden?
    * Wurden alle Transparenzregeln eingehalten (Beteiligung der Öffentlichkeit/ politische Teilhabe)?
1. Kann das Bauvorhaben im Rahmen des Antrags auf Genehmigung nach § 4 BImSchG noch verhindert werden?
1. Kann - trotz baurechtlicher Zulassung - der Betrieb am Standort verhindert werden (Betriebserlaubnis)?
1. Wie kann der laufende Betrieb dauerhaft auf Einhaltung aller Emmissionsgrenzwerte überwacht werden?

## Antrag nach Bundes-Immissionsschutzgesetz (Bezirksregierung Arnsberg)

In einer Antwort der Verwaltung der Stadt Bochum ([Vorlage Nr.: 20211758](https://session.bochum.de/bi/vo0050.asp?__kvonr=7081217)) an die Bezirksvertretung Bochum Nord vom 02.06.2021 heißt es:

> Dem Antragsteller [Fa. Ecosoil] wird empfohlen, eine Infoveranstaltung im Rahmen des Genehmigungsverfahrens nach § 4 Bundesimmissionsschutzgesetz (BImSchG), in Abstimmung mit der Bezirksregierung Arnsberg durchzuführen. Am 29.04.2021 wurde der Bezirksältestenrat darüber informiert, dass ein positiver Bescheid erteilt wird.

Der [Bescheid wurde per IFG angefordert](https://fragdenstaat.de/anfrage/bescheid-uber-die-bauvoranfrage-neuerrichtung-eines-bodenbehandlungsbetriebes/); liegt jedoch aktuell (Stand 20.06.2021) noch nicht vor.

#### Ansprechpartnerin

Bei der Bezirksregierung Arnsberg zuständig für "Abfallentsorgungsanlagen nach dem Bundes-Immissionsschutzgesetz (BImSchG)" ist [Frau Birgit Schniedermeier](https://www.bra.nrw.de/umwelt-gesundheit-arbeitsschutz/umwelt/abfallwirtschaft-und-bodenschutz/abfallentsorgungsanlagen-nach-dem-bundes-immissionsschutzgesetz-bimschg/abfallentsorgungsanlagen-nach-dem-bundes-immissionsschutzgesetz-bimschg-kontakt/stadt-bochum).

Frau Schneidermeier hat mir am 08.06.2021 telefonisch Ihre Zuständigkeit für die Firma Ecosoil bestätigt.

#### nächste Schritte

Nach Eingang des Antrags auf Genehmigung nach § 4 BImSchG der Ecosoil ist zu prüfen, welches Verfahren angewandt wird ([siehe hier, ](http://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl113s0973.pdf) Seite 3 ff.):

  * Verfahrensart "G": Genehmigungsverfahren gemäß [§ 10 BImSchG](https://dejure.org/gesetze/BImSchG/10.html) (mit Öffentlichkeitsbeteiligung)
  * Verfahrensart "V": Vereinfachtes Verfahren gemäß [§ 19 BImSchG](https://dejure.org/gesetze/BImSchG/19.html) (ohne Öffentlichkeitsbeteiligung)

## Recherche

* [Aktuelle Nachrichten zum Thema](https://www.google.de/search?q=ecosoil+Bochum+OR+Castrop+OR+Gerthe+OR+Merklinde&tbm=nws&source=lnt&tbs=sbd:1)
* [Ratsinformationssystem der Stadt Bochum](https://session.bochum.de/bi/recherche.asp)
  * [20201399: Mitteilung der Verwaltung über die Neuerrichtung eines Bodenbehandlungsbetriebes](https://session.bochum.de/bi/suchen02.asp?__swords=20201399&__sao=1&__swnot=Ausschlussworte&__zsigrnr=-none-&__axxdat_full=&__exxdat_full=&__xissort=wd&go=Suchen&__sgo=Suchen)
  * [20202696: Umwidmung des Industriegebiets zum Mischgebiet](https://session.bochum.de/bi/suchen01.asp?__swords=20202696&__sao=1&__swnot=Ausschlussworte&__zsigrnr=-none-&__axxdat_full=2020-11-01&__exxdat_full=&go=Suchen&__sgo=Suchen)
* [Bürger- und Ratsinformationssystem der Stadt Castrop-Rauxel](https://castroprauxel.more-rubin1.de/recherche/index.php)
* [Amtsblatt der Bezirksregierung Arnsberg](https://www.bra.nrw.de/bezirksregierung/amtsblatt-der-bezirksregierung-arnsberg)
